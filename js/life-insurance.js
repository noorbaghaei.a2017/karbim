
// Material Select Initialization
$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('select').niceSelect();

});
// step 1

$(document).on("change" ,function(){
    if(document.getElementById("birth-day").value !== '' &&  document.getElementById("birth-month").value !== '' &&  document.getElementById("birth-year").value !== '') {
        $("#body-next").removeAttr("disabled");
        $("#birthday-section").hide();
        $("#payment-power-section").hide();
        $("#payment-way-section").hide();
        $("#agreement-time-section").fadeIn(1000);
    }
});

$("#body-next").on("click" , function () {
    $("#birthday-section").hide();
    $("#payment-power-section").hide();
    $("#payment-way-section").hide();
    $("#agreement-time-section").fadeIn(1000);
});

$("#body-prev-1").on("click" , function () {
    $("#agreement-time-section").hide();
    $("#birthday-section").fadeIn(1000);
});

// step 2
$(document).on("change" ,function(){
    if(document.getElementById("agreement-time").value !== '' ) {
        $("#body-next-1").removeAttr("disabled");
        $("#agreement-time-section").hide();
        $("#payment-power-section").hide();
        $("#payment-way-section").fadeIn(1000);
    }
});
$("#body-next-1").on("click" , function () {
    $("#agreement-time-section").hide();
    $("#payment-way-section").fadeIn(1000);
});

$("#body-prev-2").on("click" , function () {
    $("#payment-way-section").hide();
    $("#agreement-time-section").fadeIn(1000);
});

$(document).on("change" ,function(){
    if(document.getElementById("payment-way").value !== '' ) {
        $("#body-next-2").removeAttr("disabled");
        $("#payment-way-section").hide();
        $("#payment-power-section").fadeIn(1000);
    }
});
$("#body-next-2").on("click" , function () {
    $("#payment-way-section").hide();
    $("#payment-power-section").fadeIn(1000);
});

$("#body-prev-3").on("click" , function () {
    $("#payment-power-section").hide();
    $("#payment-way-section").fadeIn(1000);
});

$(document).on("keyup" ,function(){
    if(document.getElementById("form1").value.length >= 5 && document.getElementById("form1").value >50000) {
        $("#life-inquiry").removeAttr("disabled");
    }
    else{
        let anchor = document.getElementById("life-inquiry");
        let att = document.createAttribute("disabled");
        anchor.setAttributeNode(att);
    }
});