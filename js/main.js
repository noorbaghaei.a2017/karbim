(function ($)
{ "use strict";

	$(".backtotop").addClass("hidden-top");
	$(window).scroll(function () {
		if ($(this).scrollTop() < 60) {
			$(".backtotop").addClass("hidden-top")
		} else {
			$(".backtotop").removeClass("hidden-top")
		}
	});
	$('.backtotop').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1200 , 'easeInExpo');
		return false;
	});

	/* 1. Proloder */
	$(window).on('load', function () {
		$('#preloader-active').delay(450).fadeOut('slow');
		$('body').delay(450).css({
			'overflow': 'visible'
		});
	});

	/* 2. sticky And Scroll UP */
	$(window).on('scroll', function () {
		var scroll = $(window).scrollTop();
		if (scroll < 400) {
			$(".header-sticky").removeClass("sticky-bar");
			$('#back-top').fadeOut(500);
		} else {
			$(".header-sticky").addClass("sticky-bar");
			$('#back-top').fadeIn(500);
		}
	});
	// Scroll Up
	$('#back-top a').on("click", function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});


})(jQuery);

$(document).ready(function() {
	$('select').niceSelect();
});
$(document).ready(function() {
	$(".reminder-date").pDatepicker({
		initialValue: false,
		observer: true,
		autoClose: true,
		responsive: true,
		format: 'YYYY/MM/DD',
		altField: '.observer-example-alt',
		onSelect: function (e) {
           console.log(e);
		}
	});
});

$(document).on("select" , function () {
  if(document.getElementById("form1").value !== ''){
  	console.log("hello")
  }
})

AOS.init({
 	duration: 800,
 	easing: 'slide',
 	once: false
 });

jQuery(document).ready(function($) {

	"use strict";

	$(window).load(function() {
		$(".loader").delay(1000).fadeOut("slow");
  	$("#overlayer").delay(1000).fadeOut("slow");		
	});
	

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
      $('.site-mobile-menu .has-children').each(function(){
        var $this = $(this);
        
        $this.prepend('<span class="arrow-collapse collapsed">');

        $this.find('.arrow-collapse').attr({
          'data-toggle' : 'collapse',
          'data-target' : '#collapseItem' + counter,
        });

        $this.find('> ul').attr({
          'class' : 'collapse',
          'id' : 'collapseItem' + counter,
        });

        counter++;

      });

    }, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
      var $this = $(this);
      if ( $this.closest('li').find('.collapse').hasClass('show') ) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }
      e.preventDefault();  
      
    });

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
	    var container = $(".site-mobile-menu");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	      if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
	    }
		});
	}; 
	siteMenuClone();

	// menu fixed js code
	$(window).scroll(function () {
		var window_top = $(window).scrollTop() + 1;
		if (window_top > 50) {
			$('.site-navbar-wrap').addClass('menu_fixed animated fadeInDown');
		} else {
			$('.site-navbar-wrap').removeClass('menu_fixed animated fadeInDown');
		}
	});


	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};

	$(".fire h3").click(function () {
       $(".fire").parent().removeClass("col-lg-3").addClass("col-lg-1");
	});
	// **************  insurance
	$('.insurance-carousel').owlCarousel({
		autoplay: true,
		autoplayTimeout: 5000,
		loop: true,
		nav: true,
		navText: [],
		autoplaySpeed: 1000,
		rtl: true,
		mouseDrag: true,
		dots: false,
		lazyLoad: true,
		items: 6,
		startPosition: 0,
		autoWidth: false,
		responsive: {
			0: {items: 2},
			750: {items: 3},
			900: {items:4},
			1200: {items: 6}
		}
	});
	// OWL BTNS
	$('.owl_btns .next').click(function () {
		var target = $(this).parents('.owl_btns').data('target');
		$(target).trigger('next.owl.carousel');
	});
	$('.owl_btns .prev').click(function () {
		var target = $(this).parents('.owl_btns').data('target');
		$(target).trigger('prev.owl.carousel');
	});

});