
$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('select').niceSelect();
});
// Material Select Initialization
$(document).ready(function() {
    $('.mdb-select').materialSelect();
});
$(".btn-tag").on("click" , function () {
  $(this).toggleClass('active');
});

$(".body").on("change" , function () {
 if(this.checked){
     $(".description").slideDown();
 }
 else{
     $(".description").slideUp();
 }
});
$(".third").on("change" , function () {
    if(this.checked){
        $(".third-description").slideDown();
    }
    else{
        $(".third-description").slideUp();
    }
});
$(".living").on("change" , function () {
    if(this.checked){
        $(".living-description").slideDown();
    }
    else{
        $(".living-description").slideUp();
    }
});
$(".bank").on("change" , function () {
    if(this.checked){
        $(".bank-description").slideDown();
    }
    else{
        $(".bank-description").slideUp();
    }
});