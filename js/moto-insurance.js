
// Material Select Initialization
$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('select').niceSelect();

    $("#start-date").pDatepicker({
        initialValue: false,
        autoClose: true,
        format: 'YYYY/MM/DD',
        onSelect: function (unix) {
            $("#start-ins").hide();
            $("#end-ins").fadeIn();
            $("#body-next-2").removeAttr('disabled');
        }
    });
    $("#end-date").pDatepicker({
        initialValue: false,
        autoClose: true,
        format: 'YYYY/MM/DD',
        onSelect: function (unix) {
            $("#end-ins").hide();
            $("#third-past-ins-company").fadeIn();
            $("#body-next-3").removeAttr('disabled');
        }
    });

});
// step 1

$(document).on("change" ,function(){
    if(document.getElementById("moto-type").value !== '' &&  document.getElementById("moto-year").value !== '') {
        $("#body-next").removeAttr("disabled");
        $("#body-modal-name ").hide();
        $("#start-ins").fadeIn(1000);
    }
});

$("#body-next").on("click" , function () {
    $("#body-modal-name ").hide();
    $("#start-ins").fadeIn(1000);
});

$("#body-prev-2").on("click" , function () {
    $("#start-ins").hide();
    $("#body-modal-name").fadeIn(1000);
});

$("#body-next-3").on("click" , function () {
    $("#end-ins").hide();
    $("#third-past-ins-company").fadeIn(1000);
});


// step 2
$(document).on("change" ,function(){
    if(document.getElementById("past-ins-co").value !== '') {
        $("#body-next-1").removeAttr("disabled");
        $("#third-past-ins-company").hide();
        $("#start-ins").hide();
        $("#end-ins").hide();
        $("#off-percent-amount").show();
    }
});

$("#body-prev-1").on("click" , function () {
    $("#third-past-ins-company").hide();
    $("#end-ins").fadeIn(1000);
});

$("#body-next-1").on("click" , function () {
    $("#third-past-ins-company").hide();
    $("#off-percent-amount").fadeIn(1000);
});


// step 3

$("#body-next-2").on("click" , function () {
    $("#start-ins").hide();
    $("#end-ins").fadeIn(1000);
});

$("#body-prev-3").on("click" , function () {
    $("#end-ins").hide();
    $("#start-ins").fadeIn(1000);
});


// step 4
$(document).on("change" ,function(){
    if(document.getElementById("third-off-percent").value !== '' && document.getElementById("drive-off-percent").value !== '' && document.getElementById('damage').value === '1') {
        $("#off-percent-amount").hide();
        $("#body-next-4").removeAttr("disabled");
        $("#start-ins").hide();
        $("#end-ins").hide();
        $("#third-pay-damage-contain").fadeIn();
    }
    else if(document.getElementById("third-off-percent").value !== '' && document.getElementById("drive-off-percent").value !== '' && document.getElementById('damage').value === '2'){
        $("#body-next-6").removeAttr('disabled');
        $("#body-next-6").show();
        $("#body-next-4").hide();
    }
});

$("#body-next-4").on("click" , function () {
    $("#off-percent-amount").hide();
    $("#third-pay-damage-contain").fadeIn(1000);
});

$("#body-prev-4").on("click" , function () {
    $("#third-past-ins-company").fadeIn(1000);
    $("#off-percent-amount").hide();
});

// step 5
$(document).on("change" ,function(){
    if(document.getElementById("finance-damage").value !== '' && document.getElementById("body-damage").value !== '' && document.getElementById("drive-damage").value !== '') {
        $("#body-next-5").removeAttr("disabled");
        $("#off-percent-amount").hide();
        $("#start-ins").hide();
        $("#end-ins").hide();
        $("#third-pay-damage-contain").fadeIn();
    }
});

$("#body-prev-5").on("click" , function () {
    $("#off-percent-amount").fadeIn(1000);
    $("#third-pay-damage-contain").hide();
    if(document.getElementById("third-off-percent").value !== '' && document.getElementById("drive-off-percent").value !== '' && document.getElementById('damage').value === '1'){
        $("#body-next-6").hide();
        $("#body-next-4").show();
        $("#third-pay-damage-contain").hide();
    }
});

