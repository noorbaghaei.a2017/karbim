
// Material Select Initialization
$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('select').niceSelect();
    $(document).on("change" , function () {
        if (document.getElementById("car-name").value !== ''){
            $(".select-box-disabled").show();
        }
    });
});

// step 1

$(document).on("change" ,function(){
    if(document.getElementById("car-name").value !== '' && document.getElementById("car-modal").value !== '') {
        $("#body-next").removeAttr("disabled");
        $("#body-modal-name ").hide();
        $("#body-year-build ").fadeIn(1000);
    }
});

$("#body-next").on("click" , function () {
    $("#body-modal-name ").hide();
    $("#body-year-build ").fadeIn(1000);
    $("#body-prev").removeAttr('disabled');
    $("#body-price").hide();
});
$("#body-prev-1").on("click" , function () {
    $("#body-modal-name ").fadeIn(1000);
    $("#body-year-build ").hide();
    $("#body-have-ins").hide();
    $("#body-year-ins").hide();
    $("#body-dont-ins").hide();
    $("#body-third-have-ins").hide();
    $("#body-price").hide();
});

// step 2
$(document).on("change" ,function(){
    if(document.getElementById("car-year").value !== '') {
        $("#body-next-1").removeAttr("disabled");
        $("#body-year-build").hide();
        $("#body-price").hide();
        $("#body-have-ins").fadeIn();
    }
});
$("#body-next-1").on("click" , function () {
    $("#body-year-build").hide();
    $("#body-have-ins").fadeIn();
});

// step 3
$(document).on("change" ,function(){
     if(document.getElementById("car-have-ins").value === '2'){
       $("#body-next-2").removeAttr("disabled");
        $("#body-have-ins").hide();
        $("#body-price").hide();
        $("#body-dont-ins").fadeIn();
        $(".ins-disabled").hide();
        document.getElementById('car-have-ins-company').value === "";
    }
     else if(document.getElementById("car-have-ins").value === '1'){
        $("#body-next-2").removeAttr("disabled");
        $(".ins-disabled").fadeIn();
    }
});
$(document).on("change" , function () {
 if(document.getElementById('car-have-ins-company').value !== ''){
     $("#body-have-ins").hide();
     $("#body-year-ins").fadeIn();
 }
});

$("#body-prev-2").on("click" , function () {
    $("#body-year-build").fadeIn();
    $("#body-have-ins").hide();
    $("#body-year-ins").hide();
    $("#body-dont-ins").hide();
    $("#body-third-have-ins").hide();
    $("#body-price").hide();
});
$("#body-next-2").on("click" , function () {
    if(document.getElementById("car-have-ins").value === '1'){
        $("#body-year-ins").fadeIn();
        $("#body-have-ins").hide();
    }
    else{
        $("#body-have-ins").hide();
        $("#body-dont-ins").fadeIn();
    }
});

// step 4
$(document).on("change" ,function(){
    if(document.getElementById("car-year-ins").value !== '') {
        $("#body-next-3").removeAttr("disabled");
        $("#body-year-ins").hide();
        $("#body-dont-ins").fadeIn();
        $("#body-price").hide();
    }
});

$("#body-next-3").on("click" , function () {
    $("#body-dont-ins").fadeIn();
    $("#body-year-ins").hide();
});
$("#body-prev-3").on("click" , function () {
    $("#body-have-ins").fadeIn();
    $("#body-year-ins").hide();
    $("#body-dont-ins").hide();
    $("#body-third-have-ins").hide();
    $("#body-price").hide();
});

// step 5
$(document).on("change" ,function(){
    if(document.getElementById("car-dont-ins").value !== '') {
        $("#body-next-4").removeAttr("disabled");
        $("#body-dont-ins").hide();
        $("#body-third-have-ins").fadeIn();
        $("#body-price").hide();
    }
});
$("#body-next-4").on("click" , function () {
    $("#body-third-have-ins").fadeIn();
    $("#body-dont-ins").hide();
});
$("#body-prev-4").on("click" , function () {
    $("#body-have-ins").fadeIn();
    $("#body-dont-ins").hide();
    $("#body-third-have-ins").hide();
    $("#body-price").hide();
});
// step 6
$(document).on("change" ,function(){
    if(document.getElementById("car-third-have-ins").value !== '') {
        $("#body-next-5").removeAttr("disabled");
        $("#body-third-have-ins").hide();
        $("#body-price").fadeIn();
    }
});
$("#body-next-5").on("click" , function () {
    $("#body-third-have-ins").hide();
    $("#body-price").fadeIn();
});
$("#body-prev-5").on("click" , function () {
    $("#body-third-have-ins").hide();
    $("#body-dont-ins").fadeIn();
    $("#body-price").hide();
});

// step 7
$(document).on("keyup" ,function(){
    if(document.getElementById("form1").value.length > 6 && document.getElementById("form1").value >5000000) {
        $("#body-next-6").removeAttr("disabled");
    }
    else{
        let anchor = document.getElementById("body-next-6");
        let att = document.createAttribute("disabled");
        anchor.setAttributeNode(att);
    }
});
$("#body-prev-6").on("click" , function () {
    $("#body-third-have-ins").fadeIn();
    $("#body-price").hide();
});