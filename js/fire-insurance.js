
// Material Select Initialization
$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('select').niceSelect();


});
$(document).on("change" , function () {
    if(document.getElementById('home-type').value === '1' || document.getElementById('home-type').value === '2'){
        $("#home-type-section").hide();
        $("#structure-type-section").fadeIn();
    }
    else if(document.getElementById('home-type').value === '3'){
        $("#form1").removeAttr('disabled');
    }
    else{
        let anchor = document.getElementById("form1");
        let att = document.createAttribute("disabled");
        anchor.setAttributeNode(att);
    }
});
$(document).on("keyup" , function () {
  if(document.getElementById("form1").value > 0 && document.getElementById("form1").value !== ''){
      $("#body-next").removeAttr('disabled');
      $("#jewelry-section").hide();
  }
  else{
      let anchor = document.getElementById("body-next");
      let att = document.createAttribute("disabled");
      anchor.setAttributeNode(att);
  }
});

$("#body-prev-1").on("click" , function () {
  $("#structure-type-section").hide();
    $("#jewelry-section").hide();
  $("#home-type-section").fadeIn();
    $("#body-next").removeAttr('disabled');
});
$("#body-next").on("click" , function () {
    $("#home-type-section").hide();
    $("#structure-type-section").fadeIn();
});

// step 2
$(document).on("change" , function () {
  if(document.getElementById('structure-type').value !== ''){
      $("#body-next-1").removeAttr('disabled');
      $("#structure-type-section").hide();
      $("#area-section").fadeIn();
  }
});
$("#body-prev-2").on("click" , function () {
    $("#area-section").hide();
    $("#structure-type-section").fadeIn();
});
$("#body-next-1").on("click" , function () {
    $("#structure-type-section").hide();
    $("#area-section").fadeIn();
});

// step3
$(document).on("keyup" , function () {
    if(document.getElementById("area").value > 0 && document.getElementById("area").value !== ''){
        $("#body-next-2").removeAttr('disabled');
    }
    else{
        let area = document.getElementById("body-next-2");
        let btn_att = document.createAttribute("disabled");
        area.setAttributeNode(btn_att);
    }
});
$("#body-prev-3").on("click" , function () {
    $("#lifetime-section").hide();
    $("#area-section").fadeIn();
});
$("#body-next-2").on("click" , function () {
    $("#area-section").hide();
    $("#lifetime-section").fadeIn();
});

// step 4
$(document).on("change" , function () {
    if(document.getElementById('lifetime').value !== ''){
        $("#body-next-3").removeAttr('disabled');
        $("#lifetime-section").hide();
        $("#area-section").hide();
        $("#jewelry-section").fadeIn();
    }
});
$("#body-prev-4").on("click" , function () {
    $("#jewelry-section").hide();
    $("#lifetime-section").fadeIn();
});
$("#body-next-3").on("click" , function () {
    $("#lifetime-section").hide();
    $("#jewelry-section").fadeIn();
});

// step 5
$(document).on("keyup" , function () {
    if(document.getElementById("furniture").value > 0 && document.getElementById("furniture").value !== ''){
        $("#area-section").hide();
        $("#end-fire").removeAttr('disabled');
    }
    else{
        $("#area-section").hide();
        let anchor = document.getElementById("body-next-4");
        let att = document.createAttribute("disabled");
        anchor.setAttributeNode(att);
    }
});
$("#body-next-4").on("click" , function () {
    $("#area-section").hide();
    location.href = "compare.html";
});