$(document).ready(function() {
    /* 1. Proloder */
    $(window).on('load', function () {
        $('#preloader-active').delay(450).fadeOut('slow');
        $('body').delay(450).css({
            'overflow': 'visible'
        });
    });

    /* 2. sticky And Scroll UP */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 400) {
            $(".header-sticky").removeClass("sticky-bar");
            $('#back-top').fadeOut(500);
        } else {
            $(".header-sticky").addClass("sticky-bar");
            $('#back-top').fadeIn(500);
        }
    });
    // Scroll Up
    $('#back-top a').on("click", function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('select').niceSelect();
});
// Material Select Initialization
$(document).ready(function() {
    $('.mdb-select').materialSelect();
});
$(".btn-tag").on("click" , function () {
    $(this).toggleClass('active');
});
$("#edit-profile").on("click" , function () {
    $(".profile-value-contain").hide();
    $(".profile-edit-contain").fadeIn();
});
$("#complete-profile").on("click" , function () {
    $(".profile-edit-contain").hide();
    $(".profile-value-contain").fadeIn();
});
$(document).on("change" , function () {
  if(document.getElementById('province-select').value !== ''){
      $(".city-select").fadeIn();
  }
});
$(".delete-status").on("click" , function () {
  $(this).parent().parent().parent().remove();
});